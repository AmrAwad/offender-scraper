from Tkinter import *
from tkFileDialog import askdirectory
import tkMessageBox as messagebox
import threading
from app import Scraper

thread = None

class GUI(Frame):

    def __init__(self,master=None):
        Frame.__init__(self, master)
        self.master = master
        self.init_window()
        self.save_dir = StringVar()

    # Creation of init_window
    def init_window(self):
        # changing the title of our master widget
        self.master.title("Sex Offender Finder")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)

        zip_code_label = Label(self, text="Enter Zip Code: ")
        zip_code_label.place(x=80, y=40)

        zip_code_input = Entry(self)
        zip_code_input.place(x=240, y=40)

        status_label = Label(self, text="Status: ")
        status_label.place(x=200, y=300)

        progress_label = Label(self, text="")
        progress_label.place(x=180, y=350)

        def update_progress_status(value):
            progress_label["text"] = value
            start_button.configure(state=NORMAL)
            if value == "Saved.":
                status_label['text'] = "Status: Scraping completed"
                messagebox.showinfo("Completed", "Scraping is completed and saved.")

        def browse_directory():
            filename = askdirectory()
            self.save_dir.set(filename)
            browse_directory_label['text'] = "Save Directory: " + filename

        def start_scraping():
            zip_code = zip_code_input.get()
            output_dir = browse_directory_label['text']
            output_dir = output_dir.replace("Save Directory:", "").strip()

            if not zip_code:
                messagebox.showinfo("Input Missing", "Please enter a zip code")
                return

            if not output_dir:
                messagebox.showinfo("Input Missing", "Output directory is not specified")
                return
            start_button.configure(state=DISABLED)
            status_label['text'] = "Status: Scraping started..."
            scraper = Scraper(zip_code=zip_code, out_file=output_dir)
            # scraper.start_scraping()
            global thread
            if not thread:
                thread = threading.Thread(target=scraper.start_scraping, args=(update_progress_status,))
                thread.start()

        browse_button = Button(self, text="Browse Output Directory", command=browse_directory)
        browse_button.place(x=80, y=150)

        browse_directory_label = Label(self, text="Save Directory: ")
        browse_directory_label.place(x=80, y=190)

        # # creating a button instance
        # quitButton = Button(self, text="Quit", command=self.client_exit)
        #
        # # placing the button on my window
        # quitButton.place(x=300, y=400)

        start_button = Button(self, text="Start", command=start_scraping)
        start_button.place(x=200, y=400)

    def client_exit(self):
        exit()

if __name__ == "__main__":
    root = Tk()
    root.geometry("700x500")
    guiFrame = GUI(root)
    root.mainloop()
