# Offender Scraper

This scraper gathers the images and information of sex offenders within a zip code from [Texas Public Sex Offender Registery](https://records.txdps.state.tx.us/SexOffenderRegistry)

The script was developed in Python 2.7, using Selenium & BeautifulSoup

## Running the script

Run the `program.bat` file which launches a simple GUI to get a zip code and output directory, then hit start to the scraper.

Here's a simple screenshot of the GUI:

![GUI Screenshot](imgs/screenshot.png)

## Testing

You can use the zip code `77088` to get some sample results.