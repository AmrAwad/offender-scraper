import os
import csv
import time
import urllib
from random import randint

from .parser import Parser
from .browser import WebBrowser


class Scraper(object):
    def __init__(self, zip_code, out_file):
        self.zip_code = zip_code
        self.output_path = out_file
        self.parser = Parser()

    def extract_file_extension(self, file_name):
        return file_name[file_name.rindex(".") + 1:]

    def download_image(self, image_url, file_name):
        # file_extension = self.extract_file_extension(image_url)
        save_file_name = file_name# + "_%s" % str(uuid.uuid4())
        save_file_name += ".png"
        download_folder = 'download'
        save_dir = os.path.join(self.output_path, download_folder)
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
        save_path = os.path.join(save_dir, save_file_name)
        urllib.urlretrieve(image_url, save_path)
        return save_path

    def save_output(self, save_file_name, data_list):
        save_file_path = os.path.join(self.output_path, save_file_name)
        with open(save_file_path, "w") as f:
            writer = csv.writer(f)
            writer.writerows(data_list)

    def start_scraping(self, progress_method=None, **kwargs):
        search_url = "https://records.txdps.state.tx.us/SexOffenderRegistry/Search/Default/SearchByZipCodes?Zipcode=%s" % self.zip_code
        if progress_method:
            progress_method("Searching for %s" % self.zip_code)
        self.browser = WebBrowser()
        self.browser.open(search_url)

        max_sleep = 20 #10 seconds

        sleep_taken = 0
        page_loaded = False
        while(sleep_taken < max_sleep):
            export_table_element = self.browser.get_element_by_id("tblExportDownloads")
            if export_table_element:
                page_loaded = True
                break
            else:
                time.sleep(1)
            sleep_taken += 1

        page = self.browser.get_page()

        offender_list = []

        all_offenders = self.parser.parse_all_offenders(page=page)

        if progress_method:
            progress_method("Total %s records found. Fetching details..." % (len(all_offenders),))

        for index, offender in enumerate(all_offenders):

            random_time = randint(2,8)
            time.sleep(random_time)
            detail_url = offender['detail_link']
            self.browser.open(detail_url)
            detail_page = self.browser.get_page()

            offender_detail = self.parser.parse_details(page=detail_page)
            if progress_method:
                progress_method("Fetching detail %s of %s" % (index + 1, len(all_offenders)))
            name = offender_detail.get('name')
            image_url = offender_detail.get('image_url')
            save_path = None
            if image_url and name:
                offense_as_filename = "".join( x for x in offender_detail.get('offense', '') if (x.isalnum() or x in "._- "))
                save_path = self.download_image(image_url, name + offense_as_filename.replace('Offense', ' -'))

            offender_name, offense, address, image_saved = '', '', '', ''
            if name:
                offender_name = name
            if offender.get('address'):
                address = offender['address']
            if offender_detail.get('offense'):
                offense = offender_detail['offense']
            if save_path:
                image_saved = save_path

            offender_list += [[offender_name, address, offense, image_saved]]

        if progress_method:
            progress_method("Saving...")

        self.save_output("data.csv", offender_list)

        if progress_method:
            progress_method("Saved.")

        self.browser.close()
