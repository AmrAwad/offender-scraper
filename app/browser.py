import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class WebBrowser(object):
    def __init__(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        # chromedriver = os.path.join(os.path.dirname(os.path.realpath(__file__)),'chromedriver.exe')
        # os.environ["webdriver.chrome.driver"] = chromedriver
        # self.driver = webdriver.Chrome(chromedriver, chrome_options=options)
        self.driver = webdriver.Chrome(chrome_options=options)

    def open(self, url):
        time.sleep(3)
        self.driver.get(url)

    def type_to(self, element, text):
        element.clear()
        element.send_keys(text)

    def send_enter_to(self, element):
        element.send_keys(Keys.ENTER)

    def get_element_by_name(self, element_name):
        element = self.driver.find_element_by_name(element_name)
        return element

    def get_element_by_id(self, element_id):
        element = self.driver.find_element_by_id(element_id)
        return element

    def get_page(self):
        return self.driver.page_source

    def close(self):
        self.driver.quit()
