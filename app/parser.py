from bs4 import BeautifulSoup


class Parser(object):
    @staticmethod
    def parse_all_offenders(page):
        offender_list = []
        soup = BeautifulSoup(page)
        the_result_table = soup.find_all('table', {'id': 'tblExportDownloads'})
        if the_result_table:
            the_result_table = the_result_table[len(the_result_table) - 1]
            the_result_body = the_result_table.find_all('tbody')
            if the_result_body:
                the_result_body = the_result_body[0]
                table_rows = the_result_body.find_all('tr')
                for row in table_rows:
                    columns = row.find_all('td')
                    if columns and len(columns) == 5:
                        first_column = columns[0]
                        last_column = columns[4]
                        detail_anchor = first_column.find_all('a', href=True)
                        if detail_anchor:
                            detail_link = detail_anchor[0]['href']

                            offender_list += [
                                {
                                    "detail_link": "https://records.txdps.state.tx.us" + detail_link,
                                    "address": last_column.text.replace('\n', '').strip()
                                }
                            ]
        return offender_list

    @staticmethod
    def parse_details(page):
        details = {}
        soup = BeautifulSoup(page)
        all_h1_heading_elements = soup.find_all('h1')
        if all_h1_heading_elements:
            offender_name = all_h1_heading_elements[len(all_h1_heading_elements) - 1].text.strip().replace('\n', '')

            details['name'] = offender_name

        all_h2_heading_elements = soup.find_all('h2')
        if all_h2_heading_elements:
            offense = None
            for h2_element in all_h2_heading_elements:
                offense = h2_element.text.strip().replace('\n', '').replace("'", "")
                if "Offense:" in offense:
                    offense = offense
                    break
            if offense:
                details['offense'] = offense

        offender_image_div_elements = soup.find_all('div', {'id': 'mugshot'})
        if offender_image_div_elements:
            offender_image_div_element = offender_image_div_elements[0]
            offender_image_img_elements = offender_image_div_element.find_all('img', {'class': 'photo'})
            if offender_image_img_elements:
                offender_image_img_element = offender_image_img_elements[0]
                image_url = offender_image_img_element['src']

                details['image_url'] = "https://records.txdps.state.tx.us" + image_url

        return details
